# Ejercicio 6
Comandos que utilizaremos: [Manual](Manual.md)

## Revertir un cambio que no ha sido "commiteado"

* Editemos nuestra clase SayayinDTO.java con cualquier modificación que queramos. En mi caso crearé un método "transformar" adicional en la clase:

```java
public void transformar(){
	//AQUI HAY MUCHA LOGICA PROGRAMADA
}
```

* Justo en el momento que ya habíamos comenzado a crear nuestro método y guardado en el sistema de archivo el arquitecto del proyecto nos indica que esa clase y el método deberá ser abstracto porque quieren añadir herencia en un futuro con clases como SuperSayayin y Ozaru. Por suerte no habíamos hecho commit a estos cambios, ¿como regresamos a la versión anterior?

```
git checkout HEAD -- 01.Ejercicios/SayayinDTO.java
```

* Revisamos la clase y la misma deberá tener el mismo contenido que tenía en el commit anterior.

## Revertir un commit

* Hagamos las modificaciones que el arquitecto nos solicitó:

```java
public class abstract SayayinDTO {
  private String nombre;
  private Integer nivelPoder;
  private static final Integer OVER_POWERED = 8000;
  
  ...
  
  public abstract void transformar();
```

* Hagamos un add y commit de este cambio para salvar nuestro trabajo ya que es final del día y ya nos vamos a casa:

```
git add .
git commit -m "Modificaciones solicitadas por el arquitecto"
```

* Al día siguiente el arquitecto te vuelve a llegar y te dice "¿sabes qué? Consulté con la almohada y creo que deberíamos hacer una interfaz en lugar de lo que te comenté ayer". Y ahora... ¿cómo reverso mis commits?
* Utiliza git log para identificar el hash del commit anterior.

```
git log
```

* Luego hacemos revert de este commit

```
git revert [HASH_DEL_COMMIT]
```

* En la pestaña que se nos abre con el mensaje del commit es un vi de unix. Presionamos esc luego ":" y seguido wq y presionamos enter.
* Revisamos la clase SayayinDTO, ya no tendrá los cambios que habíamos hecho commit ateriormente.

Existe muchas posibilidades y comandos para revertir commits. Te recomiendo que pases por el [Manual](Manual.md) de este ejercicio y comiences a jugar con estos una vez finalizados estos ejercicios o cuando tengas un tiempo en casa.